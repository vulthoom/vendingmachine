package com.state;

import org.junit.Test;

import static com.helpers.TestCaseExtended.assertImplementsInterface;

public class SelectionMadeStateTest {

    @Test
    public void shouldImplementStateInterface() throws Exception {
        assertImplementsInterface(StateInterface.class, SelectionMadeState.class);
    }
}
