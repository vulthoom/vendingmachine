package com.state;

import com.app.ConsoleManager;
import com.dataTypes.Coin;
import com.dataTypes.Inventory;
import com.dataTypes.UserMoney;
import com.helpers.TestHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static com.helpers.TestCaseExtended.assertImplementsInterface;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ConsoleManager.class)
public class EnoughMoneyStateTest {

    private EnoughMoneyState enoughMoneyState;
    private UserMoney mockUserMoney;
    private Inventory mockInventory;
    private StateManager stateManager;

    @Before
    public void setUp() throws Exception {

        TestHelper.swallowConsoleOutputForTests();
        PowerMockito.mockStatic(ConsoleManager.class);
        mockUserMoney = Mockito.mock(UserMoney.class);
        mockInventory = Mockito.mock(Inventory.class);
        stateManager = Mockito.mock(StateManager.class);
        enoughMoneyState = new EnoughMoneyState(stateManager, mockUserMoney, mockInventory);
    }

    @Test
    public void shouldImplementStateInterface() throws Exception {
        assertImplementsInterface(StateInterface.class, EnoughMoneyState.class);
    }

    @Test
    public void insertCoin_shouldDisplayCurrentTotalAfterIncrementing() throws Exception {
        int expectedTotal = 80;
        Mockito.doReturn(expectedTotal).when(mockUserMoney).getTotal();

        enoughMoneyState.insertCoin(Coin.NICKEL);

        Mockito.verify(mockUserMoney).addMoney(Coin.NICKEL);
        PowerMockito.verifyStatic();
        ConsoleManager.showCurrentTotal(expectedTotal);
    }

    public void ejectCoin_shouldReturnAllCoinsShowTotalShowInsertCoinTransitionToNotEnoughMoneyState() throws Exception {
        NotEnoughMoneyState notEnoughMoneyState = Mockito.mock(NotEnoughMoneyState.class);
        Mockito.doReturn(notEnoughMoneyState).when(this.stateManager).getNotEnoughMoneyState();
        Mockito.doReturn(85).when(this.mockUserMoney).getTotal();
        enoughMoneyState.ejectCoin();

        InOrder inOrder = Mockito.inOrder(mockUserMoney);

        inOrder.verify(mockUserMoney).returnAllCoins();
        PowerMockito.verifyStatic();
        ConsoleManager.showCurrentTotal(this.mockUserMoney.getTotal());
        PowerMockito.verifyStatic();
        ConsoleManager.showInsertCoin();
        inOrder.verify(this.stateManager).setState(notEnoughMoneyState);
    }

    @Test
    public void makeSelection_shouldVendItemAndReturnExtraCoinsAndDecrementStock_ifChangeIsPossibleAndInventoryExists() throws Exception {
        Mockito.doReturn(true).when(mockUserMoney).subtractMoney(Mockito.anyInt());
        Mockito.doReturn(true).when(mockInventory).hasInventoryLeft(Inventory.Item.COLA);
        NotEnoughMoneyState notEnoughMoneyState = Mockito.mock(NotEnoughMoneyState.class);
        Mockito.doReturn(notEnoughMoneyState).when(this.stateManager).getNotEnoughMoneyState();
        this.enoughMoneyState.makeSelection(Inventory.Item.COLA);

        Mockito.verify(this.mockUserMoney).subtractMoney(Inventory.Item.COLA.getCost());

        PowerMockito.verifyStatic();
        ConsoleManager.showVendMessage(Inventory.Item.COLA);
        Mockito.verify(this.mockInventory).decrementInventory(Inventory.Item.COLA);
        Mockito.verify(this.mockUserMoney).returnAllCoins();
        PowerMockito.verifyStatic();
        ConsoleManager.showThankYou();

        Mockito.verify(stateManager).setState(notEnoughMoneyState);
    }

    @Test
    public void makeSelection_shouldNotVendItem_ifChangeIsPossibleAndInventoryDoesNotExist() throws Exception {
        Mockito.doReturn(true).when(mockUserMoney).subtractMoney(Mockito.anyInt());
        Mockito.doReturn(false).when(mockInventory).hasInventoryLeft(Inventory.Item.COLA);
        Mockito.doReturn(15).when(mockUserMoney).getTotal();
        NotEnoughMoneyState notEnoughMoneyState = Mockito.mock(NotEnoughMoneyState.class);
        Mockito.doReturn(notEnoughMoneyState).when(this.stateManager).getNotEnoughMoneyState();
        this.enoughMoneyState.makeSelection(Inventory.Item.COLA);

        PowerMockito.verifyStatic();
        ConsoleManager.showSoldOut();
        PowerMockito.verifyStatic();
        ConsoleManager.showCurrentTotal(15);
    }

    @Test
    public void makeSelection_shouldNotVendItem_ifSoldOutAndNotEnoughMoney() throws Exception {
        Mockito.doReturn(true).when(this.mockInventory).hasInventoryLeft(Inventory.Item.CANDY);
        Mockito.doReturn(false).when(this.mockUserMoney).subtractMoney(Inventory.Item.CANDY.getCost());
        Mockito.doReturn(5).when(this.mockUserMoney).getTotal();
        NotEnoughMoneyState notEnoughMoneyState = Mockito.mock(NotEnoughMoneyState.class);
        Mockito.doReturn(notEnoughMoneyState).when(this.stateManager).getNotEnoughMoneyState();

        this.enoughMoneyState.makeSelection(Inventory.Item.CANDY);

        PowerMockito.verifyStatic();
        ConsoleManager.showCostDifference(5, Inventory.Item.CANDY.getCost());
        Mockito.verify(this.stateManager).setState(notEnoughMoneyState);
    }

    @Test
    public void makeSelection_shouldReturnChange_ifChangeIsNotPossible() throws Exception {
        NotEnoughMoneyState notEnoughMoneyState = Mockito.mock(NotEnoughMoneyState.class);
        Mockito.doReturn(notEnoughMoneyState).when(this.stateManager).getNotEnoughMoneyState();
        Mockito.doReturn(115).when(mockUserMoney).getTotal();

        Mockito.doReturn(false).when(mockUserMoney).subtractMoney(Mockito.anyInt());
        Mockito.doReturn(true).when(this.mockInventory).hasInventoryLeft(Inventory.Item.COLA);

        this.enoughMoneyState.makeSelection(Inventory.Item.COLA);

        InOrder inOrder = Mockito.inOrder(this.mockUserMoney, stateManager);

        PowerMockito.verifyStatic();
        ConsoleManager.showExactChangeMessage();
        inOrder.verify(this.mockUserMoney).returnAllCoins();
        inOrder.verify(this.stateManager).setState(notEnoughMoneyState);
        PowerMockito.verifyStatic();
        ConsoleManager.showCurrentTotal(this.mockUserMoney.getTotal());
    }
}
