package com.state;

import com.app.ConsoleManager;
import com.dataTypes.Coin;
import com.dataTypes.Inventory;
import com.dataTypes.UserMoney;
import com.helpers.TestHelper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static com.helpers.TestCaseExtended.assertImplementsInterface;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ConsoleManager.class)
public class NotEnoughMoneyStateTest {

    NotEnoughMoneyState notEnoughMoneyState;
    private UserMoney mockUserMoney;
    private Inventory mockInventory;
    private StateManager stateManager;

    @Before
    public void setUp() throws Exception {
        TestHelper.swallowConsoleOutputForTests();
        PowerMockito.mockStatic(ConsoleManager.class);
        mockUserMoney = Mockito.mock(UserMoney.class);
        mockInventory = Mockito.mock(Inventory.class);
        stateManager = Mockito.mock(StateManager.class);
        notEnoughMoneyState = new NotEnoughMoneyState(stateManager, mockUserMoney, mockInventory);
    }

    @After
    public void tearDown() throws Exception {
        TestHelper.restoreConsoleOutput();

    }

    @Test
    public void shouldImplementStateInterface() throws Exception {
        assertImplementsInterface(StateInterface.class, NotEnoughMoneyState.class);
    }

    @Test
    public void shouldInitiallyShowIntroAndInsertCoinMessages() throws Exception {
        PowerMockito.verifyStatic();
        ConsoleManager.showIntroMessage();

        PowerMockito.verifyStatic();
        ConsoleManager.showCommandOptions();

        PowerMockito.verifyStatic();
        ConsoleManager.showInsertCoin();
    }

    @Test
    public void insertCoin_shouldDisplayCurrentTotalAfterIncrementing() throws Exception {
        int expectedTotal = 80;
        Mockito.doReturn(expectedTotal).when(mockUserMoney).getTotal();

        notEnoughMoneyState.insertCoin(Coin.NICKEL);


        Mockito.verify(mockUserMoney).addMoney(Coin.NICKEL);

        PowerMockito.verifyStatic();
        ConsoleManager.showCurrentTotal(expectedTotal);
    }

    @Test
    public void ejectCoin_shouldReturnAllCoinsShowCurrentTotalShowInsertCoin() throws Exception {
        Mockito.doReturn(85).when(this.mockUserMoney).getTotal();
        notEnoughMoneyState.ejectCoin();

        InOrder inOrder = Mockito.inOrder(mockUserMoney);

        inOrder.verify(mockUserMoney).returnAllCoins();
        PowerMockito.verifyStatic();
        ConsoleManager.showCurrentTotal(this.mockUserMoney.getTotal());
        PowerMockito.verifyStatic(Mockito.times(2));
        ConsoleManager.showInsertCoin();
    }

    @Test
    public void makeSelection_ifEnoughMoneyToSelectItem_TransitionToEnoughMoneyStateMakeSelection() throws Exception {
        Mockito.doReturn(100).when(this.mockUserMoney).getTotal();

        notEnoughMoneyState.makeSelection(Inventory.Item.CANDY);

        Mockito.verify(this.stateManager).setState(this.stateManager.getEnoughMoneyState());
        Mockito.verify(this.stateManager).makeSelection(Inventory.Item.CANDY);
    }

    @Test
    public void makeSelection_ifExactlyEnoughMoneyToSelectItem_TransitionToEnoughMoneyStateMakeSelection() throws Exception {
        Mockito.doReturn(Inventory.Item.CANDY.getCost()).when(this.mockUserMoney).getTotal();

        notEnoughMoneyState.makeSelection(Inventory.Item.CANDY);

        Mockito.verify(this.stateManager).setState(this.stateManager.getEnoughMoneyState());
        Mockito.verify(this.stateManager).makeSelection(Inventory.Item.CANDY);
    }

    @Test
    public void makeSelection_ifNotEnoughMoneyToSelectItem_ShowErrorAndCurrentTotal() throws Exception {
        int total = 25;
        Mockito.doReturn(total).when(this.mockUserMoney).getTotal();

        notEnoughMoneyState.makeSelection(Inventory.Item.CANDY);

        PowerMockito.verifyStatic();
        ConsoleManager.showCostDifference(total, Inventory.Item.CANDY.getCost());
    }
}
