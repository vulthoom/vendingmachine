package com.state;

import com.app.CommandInterpreter;
import com.app.ConsoleManager;
import com.dataTypes.Coin;
import com.dataTypes.Inventory;
import com.dataTypes.UserMoney;
import com.helpers.TestHelper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static com.helpers.TestCaseExtended.assertImplementsInterface;
import static com.helpers.TestCaseExtended.assertIsOfTypeAndGet;
import static junit.framework.TestCase.assertSame;

public class StateManagerTest {
    StateManager stateManager;
    private ConsoleManager mockConsoleManager;
    private UserMoney mockUserMoney;
    private Inventory mockInventory;

    @Before
    public void setUp() throws Exception {
        TestHelper.swallowConsoleOutputForTests();

        mockConsoleManager = Mockito.mock(ConsoleManager.class);
        mockUserMoney = Mockito.mock(UserMoney.class);
        mockInventory = Mockito.mock(Inventory.class);

        stateManager = new StateManager();

        TestHelper.swapInMock(mockUserMoney, this.stateManager, "userMoney");
        TestHelper.swapInMock(mockInventory, this.stateManager, "inventory");
    }

    @After
    public void tearDown() throws Exception {
        TestHelper.restoreConsoleOutput();

    }

    @Test
    public void implementsStateInterface() throws Exception {
        assertImplementsInterface(StateInterface.class, StateManager.class);
    }

    @Test
    public void createsCollaboratorObjectsOnConstruction() throws Exception {
        this.stateManager = new StateManager();

        assertIsOfTypeAndGet(UserMoney.class, this.stateManager.getUserMoney());
        assertIsOfTypeAndGet(Inventory.class, this.stateManager.getInventory());
        CommandInterpreter commandInterpreter = assertIsOfTypeAndGet(CommandInterpreter.class, this.stateManager.getCommandInterpreter());
        assertSame(this.stateManager, commandInterpreter.getStateManager());
        assertSame(this.stateManager.getUserMoney(), commandInterpreter.getUserMoney());
    }

    @Test
    public void createsStatesOnConstruction() throws Exception {
        this.stateManager = new StateManager();

        NotEnoughMoneyState notEnoughMoneyState = assertIsOfTypeAndGet(NotEnoughMoneyState.class, stateManager.getNotEnoughMoneyState());
        assertSame(stateManager, notEnoughMoneyState.getStateManager());

        EnoughMoneyState enoughMoneyState = assertIsOfTypeAndGet(EnoughMoneyState.class, stateManager.getEnoughMoneyState());
        assertSame(stateManager, enoughMoneyState.getStateManager());

        SelectionMadeState selectionMadeState = assertIsOfTypeAndGet(SelectionMadeState.class, stateManager.getSelectionMadeState());
        assertSame(stateManager, selectionMadeState.getStateManager());

        checkUserMoneyIsShared(this.stateManager.getUserMoney(), notEnoughMoneyState.getUserMoney(), enoughMoneyState.getUserMoney(),
                selectionMadeState.getUserMoney());
        checkInventoryIsShared(this.stateManager.getInventory(), notEnoughMoneyState.getInventory(), enoughMoneyState.getInventory(),
                selectionMadeState.getInventory());

        assertSame(notEnoughMoneyState, stateManager.getCurrentState());
    }


    private void checkUserMoneyIsShared(UserMoney stateManagerUserMoney, UserMoney... stateUserMonies) {

        for (UserMoney userMoney : stateUserMonies) {
            assertSame(stateManagerUserMoney, userMoney);
        }
    }

    private void checkInventoryIsShared(Inventory stateManagerInventory, Inventory... stateInventories) {

        for (Inventory inventory : stateInventories) {
            assertSame(stateManagerInventory, inventory);
        }
    }

    @Test
    public void eachStateChangePassedToCurrentStateForHandling_insertCoin() throws Exception {

        StateInterface mockState = Mockito.mock(StateInterface.class);
        TestHelper.swapInMock(mockState, this.stateManager, "currentState");

        this.stateManager.insertCoin(Coin.DIME);

        Mockito.verify(mockState).insertCoin(Coin.DIME);
    }

    @Test
    public void eachStateChangePassedToCurrentStateForHandling_ejectCoin() throws Exception {

        StateInterface mockState = Mockito.mock(StateInterface.class);
        TestHelper.swapInMock(mockState, this.stateManager, "currentState");

        this.stateManager.ejectCoin();

        Mockito.verify(mockState).ejectCoin();
    }

    @Test
    public void eachStateChangePassedToCurrentStateForHandling_makeSelection() throws Exception {

        StateInterface mockState = Mockito.mock(StateInterface.class);
        TestHelper.swapInMock(mockState, this.stateManager, "currentState");

        Inventory.Item expectedItem = Inventory.Item.CHIPS;
        this.stateManager.makeSelection(expectedItem);

        Mockito.verify(mockState).makeSelection(expectedItem);
    }

    @Test
    public void handleCommandPassesThroughToCommandInterpreter() throws Exception {
        CommandInterpreter commandInterpreter = TestHelper.swapInMock(Mockito.mock(CommandInterpreter.class), this.stateManager, "commandInterpreter");

        String commandToSend = "commandToSend";
        this.stateManager.handleCommand(commandToSend);

        Mockito.verify(commandInterpreter).handleCommand(commandToSend);


    }
}
