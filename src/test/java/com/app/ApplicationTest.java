package com.app;

import com.dataTypes.Inventory;
import com.dataTypes.UserMoney;
import com.helpers.TestHelper;
import com.state.StateManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static com.helpers.TestCaseExtended.assertIsOfTypeAndGet;
import static junit.framework.TestCase.assertSame;
import static org.junit.Assert.assertNull;

public class ApplicationTest {
    Application application;

    @Before
    public void setUp() throws Exception {
        TestHelper.swallowConsoleOutputForTests();
        application = new Application();
    }

    @After
    public void tearDown() throws Exception {
        TestHelper.restoreConsoleOutput();
        this.application = null;
    }

    @Test
    public void initialize_LoadsConsoleIfNotPresent() throws Exception {
        assertNull(application.getConsole());

        Application.initialize();

        assertIsOfTypeAndGet(ConsoleManager.class, application.getConsole());
        assertSame(application.getStateManager(), application.getConsole().getStateManager());
    }

    @Test
    public void initialize_LoadsUserMoneyIfNotPresent() throws Exception {
        assertNull(application.getUserMoney());

        Application.initialize();

        assertIsOfTypeAndGet(UserMoney.class, application.getUserMoney());
    }

    @Test
    public void initialize_LoadsInventoryIfNotPresent() throws Exception {
        assertNull(application.getInventory());

        Application.initialize();

        assertIsOfTypeAndGet(Inventory.class, application.getInventory());

    }

    @Test
    public void initialize_LoadsStateManagerIfNotPresent() throws Exception {
        assertNull(application.getStateManager());

        Application.initialize();

        assertIsOfTypeAndGet(StateManager.class, application.getStateManager());
    }

    @Test
    public void initialize_CallsHandleInputOnConsoleManager() throws Exception {

        ConsoleManager consoleManager = TestHelper.swapInMock(Mockito.mock(ConsoleManager.class), this.application, "consoleManager");

        application.initialize();

        Mockito.verify(consoleManager).handleInput();

    }

    @Test
    public void shouldNullStaticFieldsOnConstruction() throws Exception {
        Application.initialize();

        assertIsOfTypeAndGet(ConsoleManager.class, application.getConsole());
        assertIsOfTypeAndGet(StateManager.class, application.getStateManager());

        application = new Application();

        assertNull(application.getConsole());
        assertNull(application.getStateManager());
    }

}