package com.app;

import com.dataTypes.Inventory;
import com.helpers.TestHelper;
import com.state.StateManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

import java.io.PrintStream;
import java.util.Currency;
import java.util.Scanner;

import static com.helpers.TestCaseExtended.assertIsOfTypeAndGet;
import static junit.framework.TestCase.assertEquals;

public class ConsoleManagerTest {


    private PrintStream printStream;
    ConsoleManager consoleManager;
    private ScannerWrapper scanner;
    private SystemWrapper systemWrapper;
    private StateManager stateManager;


    @Before
    public void setUp() {
        this.printStream = TestHelper.swallowConsoleOutputForTests();
        scanner = Mockito.mock(ScannerWrapper.class);
        systemWrapper = Mockito.mock(SystemWrapper.class);
        stateManager = Mockito.mock(StateManager.class);


        consoleManager = new ConsoleManager(stateManager);

        TestHelper.swapInMock(scanner, this.consoleManager, "scanner");
        TestHelper.swapInMock(systemWrapper, this.consoleManager, "systemWrapper");
    }

    @After
    public void tearDown() {
        TestHelper.restoreConsoleOutput();
    }

    @Test
    public void constructor_shouldBuildCurrencyFormatter() throws Exception {
        assertEquals(Currency.getInstance("USD"), this.consoleManager.formatter.getCurrency());
    }

    @Test
    public void constructor_shouldCreateStatelessHelperObjects() throws Exception {
        consoleManager = new ConsoleManager(stateManager);
        ScannerWrapper scannerWrapper = assertIsOfTypeAndGet(ScannerWrapper.class, consoleManager.getScanner());

        assertIsOfTypeAndGet(Scanner.class, scannerWrapper.getScanner());

        assertIsOfTypeAndGet(SystemWrapper.class, consoleManager.getSystemWrapper());
    }

    @Test
    public void constantsShouldMatchExpectedText() throws Exception {
        assertEquals("WELCOME TO THE VENDING MACHINE!", ConsoleManager.WELCOME_MESSAGE);
        assertEquals("", ConsoleManager.EMPTY_LINE);
        assertEquals("PLEASE INSERT COIN", ConsoleManager.INSERT_COIN);
        assertEquals("CURRENT TOTAL: ", ConsoleManager.CURRENT_TOTAL);
        assertEquals("To make that selection INSERT: ", ConsoleManager.COST_DIFFERENCE);
        assertEquals("Amounts: 1.Penny 5.Nickel 10.Dime 25.Quarter 50.HalfDollar", ConsoleManager.COIN_LIST);
        assertEquals("INVALID COIN", ConsoleManager.INVALID_COIN);
        assertEquals(" inserted!", ConsoleManager.INSERTED);
        assertEquals("Invalid command! Try: " + System.lineSeparator(), ConsoleManager.INVALID_PREAMBLE);
        assertEquals("A = CHIPS ($0.50) | B = CANDY ($0.65) | C = COLA ($1.00)" + System.lineSeparator() +
                        "P = Penny | N = Nickel | D = Dime | Q = Quarter | H = Half Dollar" + System.lineSeparator() +
                        "E = Eject All Coins" + System.lineSeparator() +
                        "QUIT = Quit vending machine" + System.lineSeparator() +
                        "? = Display Options" + System.lineSeparator()
                , ConsoleManager.COMMAND_OPTIONS);
        assertEquals("Here is your: ", ConsoleManager.VEND);
        assertEquals("PLEASE INSERT MORE COINS", ConsoleManager.INSERT_MORE_COINS);
        assertEquals("Make selection or insert additional coins", ConsoleManager.SELECTION_OR_INSERT);
        assertEquals("PLEASE INSERT EXACT CHANGE!", ConsoleManager.EXACT_CHANGE);
        assertEquals("THANK YOU", ConsoleManager.THANK_YOU);
        assertEquals("SOLD OUT, try another selection", ConsoleManager.SOLD_OUT);
        assertEquals("CHANGE RETURNED: ", ConsoleManager.CHANGE_RETURNED);
    }

    @Test
    public void showIntroMessage() throws Exception {
        ConsoleManager.showIntroMessage();

        Mockito.verify(this.printStream).println(ConsoleManager.WELCOME_MESSAGE);
    }

    @Test
    public void showCommandOptions() throws Exception {
        ConsoleManager.showCommandOptions();
        Mockito.verify(this.printStream).println(ConsoleManager.COMMAND_OPTIONS);
    }

    @Test
    public void showEmptyLine() throws Exception {
        ConsoleManager.showEmptyLine();

        Mockito.verify(this.printStream).println(ConsoleManager.EMPTY_LINE);
    }

    @Test
    public void showInsertCoin() throws Exception {
        ConsoleManager.showInsertCoin();

        Mockito.verify(this.printStream).println(ConsoleManager.INSERT_COIN);
    }

    @Test
    public void showCurrentTotal() throws Exception {
        ConsoleManager.showCurrentTotal(5);
        Mockito.verify(this.printStream).println(ConsoleManager.CURRENT_TOTAL + "$0.05");

        ConsoleManager.showCurrentTotal(55);
        Mockito.verify(this.printStream).println(ConsoleManager.CURRENT_TOTAL + "$0.55");

        ConsoleManager.showCurrentTotal(4592);
        Mockito.verify(this.printStream).println(ConsoleManager.CURRENT_TOTAL + "$45.92");
    }

    @Test
    public void showCostDifference() throws Exception {
        ConsoleManager.showCostDifference(50, 30);
        Mockito.verify(this.printStream).println(ConsoleManager.COST_DIFFERENCE + "$0.20");

    }

    @Test
    public void showInvalidCoinInserted() throws Exception {
        ConsoleManager.showInvalidCoinInserted();
        Mockito.verify(this.printStream).println(ConsoleManager.INVALID_COIN);
    }

    @Test
    public void showInvalidCommand() throws Exception {

        ConsoleManager.showInvalidCommand();
        Mockito.verify(this.printStream).println(ConsoleManager.INVALID_PREAMBLE + ConsoleManager.COMMAND_OPTIONS);
    }

    @Test
    public void showCoinReturn() throws Exception {
        ConsoleManager.showCoinReturn(100);
        Mockito.verify(this.printStream).println((ConsoleManager.CHANGE_RETURNED + "$1.00"));
    }

    @Test
    public void showVendMessage() throws Exception {
        ConsoleManager.showVendMessage(Inventory.Item.CANDY);
        Mockito.verify(this.printStream).println((ConsoleManager.VEND + Inventory.Item.CANDY.displayName()));
    }

    @Test
    public void showInertMoreCoins() throws Exception {
        ConsoleManager.showInsertMoreCoins();
        Mockito.verify(this.printStream).println((ConsoleManager.INSERT_MORE_COINS));

    }

    @Test
    public void showMakeASelectionOrInserMoreCoins() throws Exception {
        ConsoleManager.showMakeASelectionOrInsertMoreCoins();
        Mockito.verify(this.printStream).println((ConsoleManager.SELECTION_OR_INSERT));
    }

    @Test
    public void showExactChangeMessage() throws Exception {
        ConsoleManager.showExactChangeMessage();
        Mockito.verify(this.printStream).println(ConsoleManager.EXACT_CHANGE);
    }

    @Test
    public void showThankYou() throws Exception {
        ConsoleManager.showThankYou();
        Mockito.verify(this.printStream).println(ConsoleManager.THANK_YOU);
    }

    @Test
    public void showSoldOut() throws Exception {
        ConsoleManager.showSoldOut();
        Mockito.verify(this.printStream).println(ConsoleManager.SOLD_OUT);
    }

    @Test
    public void handleInput_shouldCallSystemExit_onQuit() throws Exception {

        Mockito.when(this.scanner.hasNextLine()).thenReturn(true, false);
        Mockito.when(this.scanner.nextLine()).thenReturn("quit");


        this.consoleManager.handleInput();

        InOrder inOrder = Mockito.inOrder(this.scanner, this.systemWrapper);

        inOrder.verify(this.scanner, Mockito.times(1)).hasNextLine();
        inOrder.verify(this.scanner).nextLine();
        inOrder.verify(this.systemWrapper).exit(0);
        inOrder.verify(this.scanner, Mockito.times(1)).hasNextLine();
    }

    @Test
    public void handleInput_shouldSendInputToStateManagerForHandling() throws Exception {
        Mockito.when(this.scanner.hasNextLine()).thenReturn(true, false);
        String expectedCommand = "some command";
        Mockito.when(this.scanner.nextLine()).thenReturn(expectedCommand);

        this.consoleManager.handleInput();

        Mockito.verify(this.stateManager).handleCommand(expectedCommand);

    }


}
