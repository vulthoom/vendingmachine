package com.app;

import com.dataTypes.Coin;
import com.dataTypes.Inventory;
import com.dataTypes.UserMoney;
import com.helpers.TestHelper;
import com.state.StateManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static junit.framework.TestCase.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ConsoleManager.class)
public class CommandInterpreterTest {

    private CommandInterpreter commandInterpreter;
    private StateManager stateManager;
    private UserMoney userMoney;

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(ConsoleManager.class);

        TestHelper.swallowConsoleOutputForTests();


        stateManager = Mockito.mock(StateManager.class);

        userMoney = Mockito.mock(UserMoney.class);
        this.commandInterpreter = new CommandInterpreter(stateManager, userMoney);

    }

    @After
    public void tearDown() throws Exception {
        TestHelper.restoreConsoleOutput();
    }

    @Test
    public void availableCommands() throws Exception {
        assertEquals(10, CommandInterpreter.availableCommands.size());
        assertTrue(CommandInterpreter.availableCommands.contains("n"));
        assertTrue(CommandInterpreter.availableCommands.contains("d"));
        assertTrue(CommandInterpreter.availableCommands.contains("q"));
        assertTrue(CommandInterpreter.availableCommands.contains("a"));
        assertTrue(CommandInterpreter.availableCommands.contains("b"));
        assertTrue(CommandInterpreter.availableCommands.contains("c"));
        assertTrue(CommandInterpreter.availableCommands.contains("e"));
        assertTrue(CommandInterpreter.availableCommands.contains("p"));
        assertTrue(CommandInterpreter.availableCommands.contains("h"));
        assertTrue(CommandInterpreter.availableCommands.contains("?"));
    }

    @Test
    public void handleCommand_Empty() throws Exception {
        commandInterpreter.handleCommand("");

        PowerMockito.verifyStatic();
        ConsoleManager.showInvalidCommand();
    }

    @Test
    public void handleCommand_commandNotPresent() throws Exception {
        commandInterpreter.handleCommand("p");
        PowerMockito.verifyStatic();
        ConsoleManager.showInvalidCoinInserted();
        PowerMockito.verifyStatic();
        ConsoleManager.showCurrentTotal(userMoney.getTotal());

        commandInterpreter.handleCommand("h");
        PowerMockito.verifyStatic(Mockito.times(2));
        ConsoleManager.showInvalidCoinInserted();

        commandInterpreter.handleCommand("f");
        PowerMockito.verifyStatic();
        ConsoleManager.showInvalidCommand();
    }

    @Test
    public void handleCommand_Coins() throws Exception {
        commandInterpreter.handleCommand("n");
        Mockito.verify(stateManager).insertCoin(Coin.NICKEL);

        commandInterpreter.handleCommand("d");
        Mockito.verify(stateManager).insertCoin(Coin.DIME);

        commandInterpreter.handleCommand("Q");
        Mockito.verify(stateManager).insertCoin(Coin.QUARTER);
    }

    @Test
    public void handleCommand_MakeSelection() throws Exception {
        commandInterpreter.handleCommand("a");
        Mockito.verify(stateManager).makeSelection(Inventory.Item.CHIPS);

        commandInterpreter.handleCommand("b");
        Mockito.verify(stateManager).makeSelection(Inventory.Item.CANDY);

        commandInterpreter.handleCommand("c");
        Mockito.verify(stateManager).makeSelection(Inventory.Item.COLA);

    }

    @Test
    public void handleCommand_ejectCoin() throws Exception {
        commandInterpreter.handleCommand("E");
        Mockito.verify(stateManager).ejectCoin();

    }

    @Test
    public void showHelp() throws Exception {
        commandInterpreter.handleCommand("?");
        PowerMockito.verifyStatic();
        ConsoleManager.showCommandOptions();
    }
}
