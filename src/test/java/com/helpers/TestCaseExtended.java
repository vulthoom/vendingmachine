package com.helpers;


import static junit.framework.TestCase.assertSame;
import static org.junit.Assert.assertEquals;

public class TestCaseExtended {


    public static void assertImplementsInterface(Class<?> expectedInterface, Class<?> aClass) throws Exception {
        try {
            assertSame(aClass.getInterfaces()[0], expectedInterface);
        } catch (Exception e) {
            throw (new Exception(aClass.getSimpleName() + " should implement " + expectedInterface.getSimpleName()));
        }
        assertEquals("Should only implement 1 interface. For multiples, use assertImplementsInterfaces!", aClass.getInterfaces().length, 1);

    }

    public static <T> T assertIsOfTypeAndGet(Class<T> classToReturn, Object objectToReturn) throws Exception {
        assertSame(classToReturn, objectToReturn.getClass());
        return classToReturn.cast(objectToReturn);
    }


}
