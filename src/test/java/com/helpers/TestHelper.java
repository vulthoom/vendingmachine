package com.helpers;

import org.mockito.Mockito;

import java.io.PrintStream;
import java.lang.reflect.Field;

public class TestHelper {

    private static PrintStream printStream;
    private static PrintStream realOut;

    public static <T> T swapInMock(T mockToInject, Object classToInejctMockInto, String fieldName) {

        Field declaredField = null;
        try {
            declaredField = classToInejctMockInto.getClass().getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        declaredField.setAccessible(true);

        try {
            declaredField.set(classToInejctMockInto, mockToInject);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return mockToInject;
    }

    public static PrintStream swallowConsoleOutputForTests() {

        realOut = System.out;
        printStream = Mockito.mock(PrintStream.class);
        System.setOut(printStream);

        return printStream;
    }

    public static void restoreConsoleOutput() {
        System.setOut(realOut);
    }
}
