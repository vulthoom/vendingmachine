package com.dataTypes;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertSame;

public class CoinTest {

    @Test
    public void coins() throws Exception {
        assertEquals(21, Coin.NICKEL.size());
        assertEquals(5, Coin.NICKEL.value());
        assertEquals("Nickel", Coin.NICKEL.displayName());

        assertEquals(18, Coin.DIME.size());
        assertEquals(10, Coin.DIME.value());
        assertEquals("Dime", Coin.DIME.displayName());

        assertEquals(24, Coin.QUARTER.size());
        assertEquals(25, Coin.QUARTER.value());
        assertEquals("Quarter", Coin.QUARTER.displayName());

        assertEquals(-1, Coin.INVALID.size());
        assertEquals(0, Coin.INVALID.value());
        assertEquals("INVALID", Coin.INVALID.displayName());
    }

    @Test
    public void convertSizeToCoin_shouldReturnTheCorrectCoinBasedOnSize() throws Exception {
        assertSame(Coin.NICKEL, Coin.convertCodeToCoin("n"));
        assertSame(Coin.DIME, Coin.convertCodeToCoin("d"));
        assertSame(Coin.QUARTER, Coin.convertCodeToCoin("q"));
        assertSame(Coin.INVALID, Coin.convertCodeToCoin("66"));

    }
}
