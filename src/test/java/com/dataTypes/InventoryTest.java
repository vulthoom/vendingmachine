package com.dataTypes;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.*;

public class InventoryTest {


    private Inventory inventory;

    @Before
    public void setUp() throws Exception {
        inventory = new Inventory();
    }

    @Test
    public void inventoryStartsAt2ForEachItem() throws Exception {
        assertEquals(2, inventory.getRemaining(Inventory.Item.CANDY));
        assertEquals(2, inventory.getRemaining(Inventory.Item.CHIPS));
        assertEquals(2, inventory.getRemaining(Inventory.Item.COLA));
    }

    @Test
    public void enumMembers() throws Exception {
        assertEquals(4, Inventory.Item.values().length);

        assertEquals(50, Inventory.Item.CHIPS.getCost());
        assertEquals("Chips", Inventory.Item.CHIPS.displayName());
        assertEquals("a", Inventory.Item.CHIPS.code());

        assertEquals(65, Inventory.Item.CANDY.getCost());
        assertEquals("Candy", Inventory.Item.CANDY.displayName());
        assertEquals("b", Inventory.Item.CANDY.code());

        assertEquals(100, Inventory.Item.COLA.getCost());
        assertEquals("Cola", Inventory.Item.COLA.displayName());
        assertEquals("c", Inventory.Item.COLA.code());


        assertEquals(0, Inventory.Item.INVALID.getCost());
        assertEquals("INVALID", Inventory.Item.INVALID.displayName());
        assertEquals("INVALID", Inventory.Item.INVALID.code());
    }

    @Test
    public void convertCodeToItemReturnsAppropriateItemOrInvalidItem() throws Exception {
        assertSame(Inventory.Item.CHIPS, Inventory.convertCodeToItem("a"));
        assertSame(Inventory.Item.CANDY, Inventory.convertCodeToItem("b"));
        assertSame(Inventory.Item.COLA, Inventory.convertCodeToItem("c"));
        assertSame(Inventory.Item.INVALID, Inventory.convertCodeToItem("J"));
        assertSame(Inventory.Item.INVALID, Inventory.convertCodeToItem("5"));
        assertSame(Inventory.Item.INVALID, Inventory.convertCodeToItem("-1"));
    }

    @Test
    public void decrementInventory_removesOneFromTheInventoryForAnItem() throws Exception {
        inventory.decrementInventory(Inventory.Item.CANDY);
        assertEquals(1, inventory.getRemaining(Inventory.Item.CANDY));

        inventory.decrementInventory(Inventory.Item.CHIPS);
        assertEquals(1, inventory.getRemaining(Inventory.Item.CHIPS));

        inventory.decrementInventory(Inventory.Item.COLA);
        assertEquals(1, inventory.getRemaining(Inventory.Item.COLA));

        inventory.decrementInventory(Inventory.Item.CANDY);
        assertEquals(0, inventory.getRemaining(Inventory.Item.CANDY));
    }

    @Test
    public void hasInventoryLeft_doesItemHaveInventoryLeft() throws Exception {
        assertTrue(inventory.hasInventoryLeft(Inventory.Item.CHIPS));
        inventory.decrementInventory(Inventory.Item.CHIPS);
        assertTrue(inventory.hasInventoryLeft(Inventory.Item.CHIPS));
        inventory.decrementInventory(Inventory.Item.CHIPS);
        assertFalse(inventory.hasInventoryLeft(Inventory.Item.CHIPS));
    }
}
