package com.dataTypes;

import com.app.ConsoleManager;
import com.helpers.TestHelper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

import java.io.PrintStream;
import java.util.Deque;

import static junit.framework.TestCase.*;

public class UserMoneyTest {
    UserMoney userMoney;
    private PrintStream printStream;

    @Before
    public void setUp() throws Exception {
        printStream = TestHelper.swallowConsoleOutputForTests();
        userMoney = new UserMoney();
    }

    @Test
    public void addMoney_AddsCoin() throws Exception {
        userMoney.addMoney(Coin.NICKEL);
        assertEquals(5, userMoney.getTotal());

        userMoney.addMoney(Coin.NICKEL);
        assertEquals(10, userMoney.getTotal());

        userMoney.addMoney(Coin.QUARTER);
        assertEquals(35, userMoney.getTotal());

        userMoney.addMoney(Coin.DIME);
        assertEquals(45, userMoney.getTotal());
    }

    @Test
    public void getTotal_shouldReturnCurrentTotal() throws Exception {
        userMoney.addMoney(Coin.QUARTER);
        userMoney.addMoney(Coin.QUARTER);

        assertEquals(50, userMoney.getTotal());

    }

    @Test
    public void subtractMoney_shouldRemoveTheLargestCoinsItCanFromTheAddedCoinsToMakeChange_Exact() throws Exception {
        userMoney.addMoney(Coin.QUARTER);
        userMoney.addMoney(Coin.DIME);
        userMoney.addMoney(Coin.DIME);
        userMoney.addMoney(Coin.NICKEL);

        assertTrue(userMoney.subtractMoney(50));

        assertEquals(0, userMoney.getAddedCoins().size());
        assertEquals(0, userMoney.getTotal());
    }

    @Test
    public void subtractMoney_shouldRemoveTheLargestCoinsItCanFromTheAddedCoinsToMakeChange_MoreCoinsThanCost() throws Exception {
        userMoney.addMoney(Coin.QUARTER);
        userMoney.addMoney(Coin.DIME);
        userMoney.addMoney(Coin.QUARTER);
        userMoney.addMoney(Coin.DIME);
        userMoney.addMoney(Coin.NICKEL);
        userMoney.addMoney(Coin.DIME);

        assertEquals(85, userMoney.getTotal());

        assertTrue(userMoney.subtractMoney(50));

        Deque<Coin> addedCoins = userMoney.getAddedCoins();
        assertEquals(4, addedCoins.size());

        assertEquals(35, userMoney.getTotal());

        assertEquals(Coin.DIME, addedCoins.pop());
        assertEquals(Coin.NICKEL, addedCoins.pop());
        assertEquals(Coin.DIME, addedCoins.pop());
        assertEquals(Coin.DIME, addedCoins.pop());
    }


    @Test
    public void subtractMoney_BugCase() throws Exception {
        userMoney.addMoney(Coin.QUARTER);
        userMoney.addMoney(Coin.QUARTER);
        userMoney.addMoney(Coin.DIME);
        userMoney.addMoney(Coin.QUARTER);

        assertEquals(85, userMoney.getTotal());

        assertFalse(userMoney.subtractMoney(Inventory.Item.CANDY.getCost()));

        assertEquals(85, userMoney.getTotal());

        Deque<Coin> addedCoins = userMoney.getAddedCoins();
        assertEquals(4, addedCoins.size());

        assertEquals(Coin.QUARTER, addedCoins.pop());
        assertEquals(Coin.DIME, addedCoins.pop());
        assertEquals(Coin.QUARTER, addedCoins.pop());
        assertEquals(Coin.QUARTER, addedCoins.pop());
    }

    @Test
    public void subtractMoney_BugTwo() throws Exception {
        userMoney.addMoney(Coin.DIME);
        userMoney.addMoney(Coin.DIME);
        userMoney.addMoney(Coin.NICKEL);
        userMoney.addMoney(Coin.NICKEL);
        userMoney.addMoney(Coin.DIME);
        userMoney.addMoney(Coin.QUARTER);

        assertTrue(userMoney.subtractMoney(Inventory.Item.CHIPS.getCost()));

        assertSame(Coin.DIME, userMoney.getAddedCoins().pop());
        assertSame(Coin.NICKEL, userMoney.getAddedCoins().pop());

        assertEquals(0, userMoney.getAddedCoins().size());
    }

    @Test
    public void subtractMoney_NotEnough() throws Exception {
        userMoney.addMoney(Coin.QUARTER);
        userMoney.addMoney(Coin.QUARTER);

        assertFalse(userMoney.subtractMoney(Inventory.Item.CANDY.getCost()));

        assertSame(Coin.QUARTER, userMoney.getAddedCoins().pop());
        assertSame(Coin.QUARTER, userMoney.getAddedCoins().pop());

        assertEquals(0, userMoney.getAddedCoins().size());
    }

    @Test
    public void returnAllCoins_shouldReturnAllCoinsToUser() throws Exception {
        userMoney.addMoney(Coin.NICKEL);
        userMoney.addMoney(Coin.DIME);
        userMoney.addMoney(Coin.NICKEL);
        userMoney.addMoney(Coin.QUARTER);
        userMoney.addMoney(Coin.DIME);

        assertEquals(5, userMoney.getAddedCoins().size());

        userMoney.returnAllCoins();

        assertEquals(0, userMoney.getAddedCoins().size());

        InOrder inOrder = Mockito.inOrder(this.printStream);

        inOrder.verify(this.printStream).println((ConsoleManager.CHANGE_RETURNED + "$0.55"));

    }
}
