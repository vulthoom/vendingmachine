package com.dataTypes;

import java.util.HashMap;

public enum Coin {
    NICKEL(21, 5, "Nickel"), DIME(18, 10, "Dime"),
    QUARTER(24, 25, "Quarter"), INVALID(-1, 0, "INVALID");

    private static HashMap<String, Integer> codeToSizeMap = new HashMap<String, Integer>() {{
        put("n", 21);
        put("d", 18);
        put("q", 24);
    }};

    private final int size;
    private int value;
    private String displayName;

    Coin(int size, int value, String displayName) {
        this.size = size;
        this.value = value;
        this.displayName = displayName;
    }

    public int size() {
        return size;
    }

    public int value() {
        return value;
    }

    public String displayName() {
        return this.displayName;
    }

    public static Coin convertCodeToCoin(String code) {
        if (codeToSizeMap.keySet().contains(code.toLowerCase())) {
            for (Coin coin : Coin.values()) {
                if (coin.size() == codeToSizeMap.get(code).intValue()) {
                    return coin;
                }
            }
        }
        return INVALID;
    }
}