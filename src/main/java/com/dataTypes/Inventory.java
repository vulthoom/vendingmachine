package com.dataTypes;

import java.util.HashMap;
import java.util.Map;

public class Inventory {

    Map<Item, Integer> inventoryCount;

    public Inventory() {
        this.inventoryCount = new HashMap<>();

        for (Item item : Item.values()) {
            this.inventoryCount.put(item, 2);
        }
    }

    public void decrementInventory(Item item) {
        this.inventoryCount.put(item, this.inventoryCount.get(item) - 1);
    }

    public int getRemaining(Item item) {
        return this.inventoryCount.get(item);
    }

    public boolean hasInventoryLeft(Item item) {
        return this.inventoryCount.get(item) > 0;
    }

    public enum Item {
        CHIPS(50, "Chips", "a"), CANDY(65, "Candy", "b"),
        COLA(100, "Cola", "c"), INVALID(0, "INVALID", "INVALID");

        private int cost;
        private String displayName;
        private String code;

        Item(int cost, String displayName, String code) {
            this.cost = cost;
            this.displayName = displayName;
            this.code = code;
        }

        public int getCost() {
            return cost;
        }

        public String displayName() {
            return this.displayName;
        }

        public String code() {
            return code;
        }
    }

    public static Item convertCodeToItem(String itemCode) {
        for (Item item : Item.values()) {
            if (item.code().equals(itemCode)) {
                return item;
            }
        }
        return Item.INVALID;
    }
}
