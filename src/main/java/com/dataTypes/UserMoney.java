package com.dataTypes;

import com.app.ConsoleManager;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;

public class UserMoney {
    private ArrayDeque<Coin> addedCoins;

    public UserMoney() {
        this.addedCoins = new ArrayDeque<>();
    }

    public void addMoney(Coin coin) {
        this.addedCoins.push(coin);
    }

    public int getTotal() {
        int total = 0;
        for (Coin coin : addedCoins) {
            total += coin.value();
        }
        return total;
    }

    public boolean subtractMoney(int cost) {
        ArrayDeque<Coin> originalCoins = addedCoins.clone();

        int value = 0;
        for (Coin coin : addedCoins) {
            value += coin.value();
        }

        if (value < cost) {
            return false;
        }

        while (cost > 0) {
            Coin largestCoin = findLargestCoin();

            if (cost < largestCoin.value()) {
                largestCoin = findBestCoin(cost);
                if (largestCoin.equals(Coin.INVALID)) {
                    addedCoins = originalCoins;
                    return false;
                }
            }

            cost -= largestCoin.value();
            addedCoins.removeLastOccurrence(largestCoin);
        }

        if (cost < 0) {
            addedCoins = originalCoins;
            return false;
        }

        return true;
    }

    private Coin findBestCoin(int cost) {
        Iterator<Coin> iterator = addedCoins.iterator();
        while (iterator.hasNext()) {
            Coin coin = iterator.next();
            if (cost >= coin.value()) {
                return coin;
            }
        }
        return Coin.INVALID;
    }

    private Coin findLargestCoin() {
        Coin largestCoin = Coin.INVALID;
        for (Coin coin : addedCoins) {
            if (coin.value() > largestCoin.value()) {
                largestCoin = coin;
            }
        }
        return largestCoin;
    }

    protected Deque<Coin> getAddedCoins() {
        return addedCoins;
    }

    public void returnAllCoins() {
        int coinValue = 0;
        while (!addedCoins.isEmpty()) {
            coinValue += addedCoins.pop().value();
        }
        ConsoleManager.showCoinReturn(coinValue);
    }
}
