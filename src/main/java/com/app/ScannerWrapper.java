package com.app;

import java.util.Scanner;

/**
 * Created by ifeldt on 12/20/2016.
 */
public class ScannerWrapper {

    private final Scanner scanner;

    public ScannerWrapper(Scanner scanner) {
        this.scanner = scanner;
    }

    public boolean hasNextLine() {
        return this.scanner.hasNextLine();
    }

    public String nextLine() {
        return this.scanner.nextLine();
    }

    public Scanner getScanner() {
        return scanner;
    }
}
