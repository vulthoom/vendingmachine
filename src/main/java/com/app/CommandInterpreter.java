package com.app;

import com.dataTypes.Coin;
import com.dataTypes.Inventory;
import com.dataTypes.UserMoney;
import com.state.StateManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandInterpreter {

    public static List<String> availableCommands = new ArrayList<>(Arrays.asList(
            "n", "d", "q", "a", "b", "c", "e", "p", "h", "?"
    ));

    private StateManager stateManager;
    private UserMoney userMoney;

    public CommandInterpreter(StateManager stateManager, UserMoney userMoney) {
        this.stateManager = stateManager;
        this.userMoney = userMoney;
    }

    public void handleCommand(String command) {
        command = command.toLowerCase();
        if (availableCommands.contains(command.toLowerCase())) {

            boolean coinInserted = command.matches("n|d|q");
            boolean makeSelection = command.toLowerCase().matches("a|b|c");
            boolean ejectCoin = command.toLowerCase().matches("e");

            if (coinInserted) {
                stateManager.insertCoin(Coin.convertCodeToCoin(command));
            } else if (makeSelection) {
                stateManager.makeSelection(Inventory.convertCodeToItem(command));
            } else if (ejectCoin) {
                stateManager.ejectCoin();
            } else if (command.toLowerCase().matches("p|h")) {
                ConsoleManager.showInvalidCoinInserted();
                ConsoleManager.showCurrentTotal(userMoney.getTotal());
            } else if (command.matches("\\?")) {
                ConsoleManager.showCommandOptions();
            }
        } else {
            ConsoleManager.showInvalidCommand();
        }

    }

    public StateManager getStateManager() {
        return stateManager;
    }

    public UserMoney getUserMoney() {
        return userMoney;
    }
}
