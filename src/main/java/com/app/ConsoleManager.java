package com.app;

import com.dataTypes.Inventory;
import com.state.StateManager;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.Scanner;

public class ConsoleManager {
    public static final String WELCOME_MESSAGE = "WELCOME TO THE VENDING MACHINE!";
    public static final String EMPTY_LINE = "";
    public static final String INSERT_COIN = "PLEASE INSERT COIN";
    public static final String CURRENT_TOTAL = "CURRENT TOTAL: ";
    public static final String COST_DIFFERENCE = "To make that selection INSERT: ";
    public static final String COIN_LIST = "Amounts: 1.Penny 5.Nickel 10.Dime 25.Quarter 50.HalfDollar";
    public static final String INVALID_COIN = "INVALID COIN";
    public static final String INVALID_PREAMBLE = "Invalid command! Try: " + System.lineSeparator();
    public static final String COMMAND_OPTIONS =
            "A = CHIPS ($0.50) | B = CANDY ($0.65) | C = COLA ($1.00)" + System.lineSeparator() +
                    "P = Penny | N = Nickel | D = Dime | Q = Quarter | H = Half Dollar" + System.lineSeparator() +
                    "E = Eject All Coins" + System.lineSeparator() +
                    "QUIT = Quit vending machine" + System.lineSeparator() +
                    "? = Display Options" + System.lineSeparator();
    public static final String INSERTED = " inserted!";
    public static final String VEND = "Here is your: ";
    public static final String INSERT_MORE_COINS = "PLEASE INSERT MORE COINS";
    public static final String SELECTION_OR_INSERT = "Make selection or insert additional coins";
    public static final String EXACT_CHANGE = "PLEASE INSERT EXACT CHANGE!";
    public static final String THANK_YOU = "THANK YOU";
    public static final String SOLD_OUT = "SOLD OUT, try another selection";
    public static final String CHANGE_RETURNED = "CHANGE RETURNED: ";

    static NumberFormat formatter;
    private ScannerWrapper scanner;
    private SystemWrapper systemWrapper;
    private StateManager stateManager;

    public ConsoleManager(StateManager stateManager) {
        this.stateManager = stateManager;
        this.scanner = new ScannerWrapper(new Scanner(System.in));
        this.systemWrapper = new SystemWrapper();
        formatter = buildFormatter();
    }

    public void handleInput() {
        while (scanner.hasNextLine()) {
            String input = scanner.nextLine();
            if (input.toLowerCase().equals("quit")) {
                systemWrapper.exit(0);
            } else {
                this.stateManager.handleCommand(input);
            }
        }
    }


    public static void showIntroMessage() {
        System.out.println(WELCOME_MESSAGE);
    }

    public static void showEmptyLine() {
        System.out.println(EMPTY_LINE);
    }


    public static void showInsertCoin() {
        System.out.println(INSERT_COIN);
    }

    public static void showCurrentTotal(int total) {
        System.out.println(CURRENT_TOTAL + (formatter.format(total / 100d)));

    }

    public static void showInvalidCoinInserted() {
        System.out.println(INVALID_COIN);
    }

    public static void showCostDifference(int total, int itemCost) {
        System.out.println(COST_DIFFERENCE + formatter.format((total - itemCost) / 100d));
    }


    public ScannerWrapper getScanner() {
        return scanner;
    }

    public SystemWrapper getSystemWrapper() {
        return systemWrapper;
    }

    public StateManager getStateManager() {
        return stateManager;
    }

    public static void showInvalidCommand() {
        System.out.println(INVALID_PREAMBLE + COMMAND_OPTIONS);
    }

    public static void showCommandOptions() {
        System.out.println(COMMAND_OPTIONS);
    }


    private NumberFormat buildFormatter() {
        Currency usd = Currency.getInstance("USD");
        NumberFormat format = NumberFormat.getCurrencyInstance(java.util.Locale.US);
        format.setCurrency(usd);
        return format;
    }

    public static void showCoinReturn(int coin) {
        System.out.println(CHANGE_RETURNED + (formatter.format(coin / 100d)));
    }

    public static void showVendMessage(Inventory.Item item) {
        System.out.println(VEND + item.displayName());
    }

    public static void showInsertMoreCoins() {
        System.out.println(INSERT_MORE_COINS);
    }

    public static void showMakeASelectionOrInsertMoreCoins() {
        System.out.println(SELECTION_OR_INSERT);
    }

    public static void showExactChangeMessage() {
        System.out.println(EXACT_CHANGE);
    }

    public static void showThankYou() {
        System.out.println(THANK_YOU);
    }

    public static void showSoldOut() {
        System.out.println(ConsoleManager.SOLD_OUT);

    }
}
