package com.app;

import com.dataTypes.Inventory;
import com.dataTypes.UserMoney;
import com.state.StateManager;

public class Application {

    private static ConsoleManager consoleManager;
    private static StateManager stateManager;
    private static Inventory inventory;
    private static UserMoney userMoney;

    public Application() {
        consoleManager = null;
        userMoney = null;
        inventory = null;
        stateManager = null;
    }

    public static void main(String[] args) {
        initialize();
    }


    static void initialize() {

        if (userMoney == null) {
            userMoney = new UserMoney();
        }

        if (inventory == null) {
            inventory = new Inventory();
        }

        if (stateManager == null) {
            stateManager = new StateManager();
        }

        if (consoleManager == null) {
            consoleManager = new ConsoleManager(stateManager);
        }

        consoleManager.handleInput();

    }

    ConsoleManager getConsole() {
        return consoleManager;
    }

    public StateManager getStateManager() {
        return stateManager;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public UserMoney getUserMoney() {
        return userMoney;
    }
}
