package com.state;

import com.dataTypes.Coin;
import com.dataTypes.Inventory;
import com.dataTypes.UserMoney;

public class SelectionMadeState implements StateInterface {
    private StateManager stateManager;
    private UserMoney userMoney;
    private Inventory inventory;

    public SelectionMadeState(StateManager stateManager, UserMoney userMoney, Inventory inventory) {

        this.stateManager = stateManager;
        this.userMoney = userMoney;
        this.inventory = inventory;
    }

    @Override
    public void insertCoin(Coin v) {

    }

    @Override
    public void ejectCoin() {

    }

    @Override
    public void makeSelection(Inventory.Item item) {

    }

    public StateManager getStateManager() {
        return stateManager;
    }

    public UserMoney getUserMoney() {
        return userMoney;
    }

    public Inventory getInventory() {
        return inventory;
    }
}
