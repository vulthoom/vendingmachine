package com.state;

import com.app.ConsoleManager;
import com.dataTypes.Coin;
import com.dataTypes.Inventory;
import com.dataTypes.UserMoney;

public class NotEnoughMoneyState implements StateInterface {

    private UserMoney userMoney;
    private StateManager stateManager;
    private Inventory inventory;

    public NotEnoughMoneyState(StateManager stateManager, UserMoney userMoney, Inventory inventory) {
        this.stateManager = stateManager;
        this.inventory = inventory;
        this.userMoney = userMoney;

        ConsoleManager.showIntroMessage();
        ConsoleManager.showCommandOptions();
        ConsoleManager.showInsertCoin();
    }

    @Override
    public void insertCoin(Coin coin) {
        this.userMoney.addMoney(coin);
        ConsoleManager.showCurrentTotal(this.userMoney.getTotal());
    }

    @Override
    public void ejectCoin() {
        this.userMoney.returnAllCoins();
        ConsoleManager.showCurrentTotal(this.userMoney.getTotal());
        ConsoleManager.showInsertCoin();
    }

    @Override
    public void makeSelection(Inventory.Item item) {
        if (userMoney.getTotal() >= item.getCost()) {
            this.stateManager.setState(this.stateManager.getEnoughMoneyState());
            this.stateManager.makeSelection(item);
        } else {
            ConsoleManager.showCostDifference(this.userMoney.getTotal(), item.getCost());
        }

    }

    public StateManager getStateManager() {
        return stateManager;
    }

    public UserMoney getUserMoney() {
        return userMoney;
    }

    public Inventory getInventory() {
        return inventory;
    }
}
