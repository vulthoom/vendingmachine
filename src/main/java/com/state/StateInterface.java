package com.state;

import com.dataTypes.Coin;
import com.dataTypes.Inventory;

public interface StateInterface {

    void insertCoin(Coin coin);

    void ejectCoin();

    void makeSelection(Inventory.Item item);

}
