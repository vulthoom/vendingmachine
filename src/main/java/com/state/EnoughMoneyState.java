package com.state;

import com.app.ConsoleManager;
import com.dataTypes.Coin;
import com.dataTypes.Inventory;
import com.dataTypes.UserMoney;

public class EnoughMoneyState implements StateInterface {
    private StateManager stateManager;
    private Inventory inventory;
    private UserMoney userMoney;

    public EnoughMoneyState(StateManager stateManager, UserMoney userMoney, Inventory inventory) {
        this.stateManager = stateManager;
        this.userMoney = userMoney;
        this.inventory = inventory;
    }

    @Override
    public void insertCoin(Coin coin) {
        this.userMoney.addMoney(coin);
        ConsoleManager.showCurrentTotal(this.userMoney.getTotal());
    }

    @Override
    public void ejectCoin() {
        this.userMoney.returnAllCoins();
        ConsoleManager.showCurrentTotal(this.userMoney.getTotal());
        ConsoleManager.showInsertCoin();
        this.stateManager.setState(this.stateManager.getNotEnoughMoneyState());
    }

    @Override
    public void makeSelection(Inventory.Item item) {
        boolean hasInventory = this.inventory.hasInventoryLeft(item);
        boolean hasEnoughMoney = hasInventory && this.userMoney.subtractMoney(item.getCost());

        if (!hasInventory) {
            ConsoleManager.showSoldOut();
            ConsoleManager.showCurrentTotal(this.userMoney.getTotal());
        } else if (hasEnoughMoney) {
            ConsoleManager.showVendMessage(item);
            this.inventory.decrementInventory(item);
            ConsoleManager.showThankYou();
            this.userMoney.returnAllCoins();
            this.stateManager.setState(this.stateManager.getNotEnoughMoneyState());
        } else if (item.getCost() > this.userMoney.getTotal()) {
            ConsoleManager.showCostDifference(this.userMoney.getTotal(), item.getCost());
            this.stateManager.setState(this.stateManager.getNotEnoughMoneyState());
        } else {
            ConsoleManager.showExactChangeMessage();
            this.userMoney.returnAllCoins();
            ConsoleManager.showCurrentTotal(userMoney.getTotal());
            this.stateManager.setState(this.stateManager.getNotEnoughMoneyState());

        }
    }


    public StateManager getStateManager() {
        return stateManager;
    }

    public UserMoney getUserMoney() {
        return userMoney;
    }

    public Inventory getInventory() {
        return inventory;
    }
}
