package com.state;

import com.app.CommandInterpreter;
import com.dataTypes.Coin;
import com.dataTypes.Inventory;
import com.dataTypes.UserMoney;

public class StateManager implements StateInterface {
    private StateInterface notEnoughMoneyState;
    private StateInterface enoughMoneyState;
    private StateInterface selectionMadeState;

    private StateInterface currentState;
    private Inventory inventory;
    private UserMoney userMoney;
    private CommandInterpreter commandInterpreter;

    public StateManager() {
        this.userMoney = new UserMoney();
        this.inventory = new Inventory();
        this.commandInterpreter = new CommandInterpreter(this, userMoney);

        this.notEnoughMoneyState = new NotEnoughMoneyState(this, userMoney, inventory);
        this.enoughMoneyState = new EnoughMoneyState(this, userMoney, inventory);
        this.selectionMadeState = new SelectionMadeState(this, userMoney, inventory);

        this.currentState = notEnoughMoneyState;
    }

    public void handleCommand(String command) {
        this.commandInterpreter.handleCommand(command);
    }

    @Override
    public void insertCoin(Coin coinAmount) {
        this.currentState.insertCoin(coinAmount);
    }

    @Override
    public void ejectCoin() {
        this.currentState.ejectCoin();
    }

    @Override
    public void makeSelection(Inventory.Item item) {
        this.currentState.makeSelection(item);
    }

    public StateInterface getNotEnoughMoneyState() {
        return notEnoughMoneyState;
    }

    public StateInterface getEnoughMoneyState() {
        return enoughMoneyState;
    }

    public StateInterface getSelectionMadeState() {
        return selectionMadeState;
    }

    public StateInterface getCurrentState() {
        return currentState;
    }

    public void setState(StateInterface state) {
        this.currentState = state;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public UserMoney getUserMoney() {
        return userMoney;
    }

    public CommandInterpreter getCommandInterpreter() {
        return commandInterpreter;
    }
}
